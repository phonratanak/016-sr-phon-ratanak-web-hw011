import React, { Component } from 'react'
import './App.css';
import {Card,ListGroup} from 'react-bootstrap';
export default class Result extends Component {
    render() {
        return (
            <div className="result">
                <Card className="header">
                    <Card.Body ><h2>Result History</h2></Card.Body>
                </Card>
                <ListGroup>
                    {this.props.value.map((arr)=><ListGroup.Item key={arr}>{arr}</ListGroup.Item>)}
                </ListGroup>
            </div>
        )
    }
}
