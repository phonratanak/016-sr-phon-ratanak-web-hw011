import React, { Component } from 'react'
import './App.css';
import Result from './Result';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Card,Form,Button,Container,Row,Col } from 'react-bootstrap';
export default class From extends Component {
    constructor(props){
        super(props)
        this.state = { num1: '', num2: '', total: '',list: [],value: '+',picture:'https://c7.uihere.com/files/317/996/990/math-mental-math-games-multiplication-table-automath-math-workout-duel-cool-math-facts-educational-games-for-kids-mathematics-mental-calculation-geomentry.jpg',}
      }
      calculator(){
          let totalSum;
          let valueOne=this.state.num1;
          let valueTwo=this.state.num2;
          let rem=this.state.value;
        if(!(valueOne==="" || valueTwo===""))
        {
            if (!isNaN(valueOne) && !isNaN(valueTwo)){
                let num1=parseFloat(valueOne);
                let num2=parseFloat(valueTwo);
                // eslint-disable-next-line default-case
                switch(rem)
                {
                    case "+":
                        totalSum=num1 + num2;
                        break;
                    case "-":
                        totalSum=num1 - num2;
                        break;
                    case "*":
                        totalSum=num1 * num2;
                        break;
                    case "/":
                            totalSum=num1 / num2;
                        break;
                    case "%":
                            totalSum=num1 % num2;
                        break;
                }
                this.setState({ total:totalSum });
                this.setState({num1:'',num2:''});
                this.setState({list:[...this.state.list,totalSum]});
            }
            else
            {
                alert("Your input invalid");
            }
        }
        else
        {
            alert("Your input Empty");
        }
      }
      render() {
        return (
            <Container>
                <Row className="group">
                    <Col xs lg={4}>
                        <Card className="contain">
                            <Card.Body className="Img"><img src={this.state.picture} alt="calculator"></img></Card.Body>
                            <input type="text" className="inputStyle" value={this.state.num1} onChange={ (eve) => { this.setState({ num1: eve.target.value })}}/>
                            <input type="text" className="inputStyle" value={this.state.num2} onChange={ (eve) => { this.setState({ num2: eve.target.value })}}/>
                            <Form>
                                <Form.Group controlId="exampleForm.SelectCustom" value={this.state.value} onChange={(eve)=>{this.setState({value:eve.target.value})}}>
                                    <Form.Label>Custom select</Form.Label>
                                    <Form.Control as="select" custom>
                                    <option value="+">(+) Addition</option>
                                    <option value="-">(-) Subtraction</option>
                                    <option value="*">(*) Multiplication</option>
                                    <option value="/">(/) Division</option>
                                    <option value="%">(%) Module</option>
                                    </Form.Control>
                                </Form.Group>
                            </Form>
                            <Button variant="outline-success" onClick={()=>{this.calculator()}} >Calculate</Button>
                        </Card>
                    </Col>
                    <Col xs lg={4}>
                        <Card className="contain">
                            <Result value={this.state.list}/>
                        </Card>
                    </Col>
                </Row>
            </Container>
        );
      }
}
